import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTable } from '@angular/material';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';

export interface UsersData {
  name: string;
  id: number;
}
export interface studentData{
  name:string;
  studentId:number;
  branch:string;
  emailId:string;
  district:string;
}

const ELEMENT_DATA: UsersData[] = [
  {id: 1560608769632, name: 'Artificial Intelligence'},
  {id: 1560608796014, name: 'Machine Learning'},
  {id: 1560608787815, name: 'Robotic Process Automation'},
  {id: 1560608805101, name: 'Blockchain'}
];
@Component({
  selector: 'app-mattable',
  templateUrl: './mattable.component.html',
  styleUrls: ['./mattable.component.css']
})
export class MattableComponent {
  studentData:object[]=[
    {name:'santhosh',studentId:1234,branch:'eee',emailId:'santhosh@gmail.com',district:'warangal'},
    {name:'sudhakar',studentId:1235,branch:'eee',emailId:'sudhakar@gmail.com',district:'janagam'},
    {name:'sushanth',studentId:1236,branch:'eee',emailId:'sushanth@gmail.com',district:'vijayawada'},
    {name:'sampath',studentId:1237,branch:'eee',emailId:'sampath@gmail.com',district:'hyderabad'},
    {name:'srishanvi',studentId:1238,branch:'eee',emailId:'srishanvi@gmail.com',district:'khammam'},
    {name:'sushmitha',studentId:1239,branch:'eee',emailId:'sushmitha@gmail.com',district:'karimnagar'},
    {name:'shanmukh',studentId:1240,branch:'eee',emailId:'shanmukh@gmail.com',district:'adilabad'},
    {name:'subbu',studentId:1241,branch:'eee',emailId:'subbu@gmail.com',district:'warangal'},
    {name:'shiva',studentId:1242,branch:'eee',emailId:'shiva@gmail.com',district:'nalgonda'}
  ];
  displayedColumns: string[] = ['id', 'name', 'action'];
  dataSource = ELEMENT_DATA;
  displayedColumns1:string[]=['name','studentId','branch','emailId','district','action'];
  dataSource1= this.studentData;

  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  @ViewChild('myTable1',{static:true}) table1: MatTable<any>;

  constructor(public dialog: MatDialog) {}

  openDialog1(action,obj) {
    console.log(action,obj);
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result.event,result.data)
      if(result.event == 'Add'){
        this.addRowData1(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData1(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData1(result.data);
      }
    });
  }

  openDialog(action,obj) {
    console.log(action,obj);
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result.event);
      if(result.event == 'Add'){
        this.addRowData(result.data);
        console.log(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData1(row_obj){
    console.log(row_obj)
    this.dataSource1.push(row_obj);

    console.log(this.studentData);
    console.log(this.dataSource1);
    this.table1.renderRows();
    
  }
  addRowData(row_obj){
    var d = new Date();
    this.dataSource.push({
      id:d.getTime(),
      name:row_obj.name
    });
    this.table.renderRows();
    
  }
  updateRowData1(row_obj){
    this.dataSource1 = this.dataSource1.filter((value,key)=>{
      if(value['studentId'] == row_obj.studentId){
        value['name'] = row_obj.name;
        value['emailId'] =row_obj.emailId;
        value['district'] = row_obj.district;

        value['branch']=row_obj.branch;
      }
      return true;
    });
  }
  deleteRowData1(row_obj){
    this.dataSource1 = this.dataSource1.filter((value,key)=>{
      return value['studentId'] != row_obj.studentId;
    });
  }

  updateRowData(row_obj){
    this.dataSource = this.dataSource.filter((value,key)=>{
      if(value.id == row_obj.id){
        value.name = row_obj.name;
      }
      return true;
    });
  }
  deleteRowData(row_obj){
    this.dataSource = this.dataSource.filter((value,key)=>{
      return value.id != row_obj.id;
    });
  }



}
