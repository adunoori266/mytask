import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule,MatFormFieldModule,MatInputModule } from "@angular/material";
import { MatButtonModule, MatDialogModule } from "@angular/material";
import { MattableComponent } from './mattable/mattable.component';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { MatTabsModule, MatSelectModule } from "@angular/material";
import { TaskComponent } from './task/task.component';



@NgModule({
  declarations: [
    AppComponent,
    MattableComponent,
    DialogBoxComponent,
    TaskComponent,
    
  ],
  imports: [
    BrowserModule,FormsModule, ReactiveFormsModule,MatTableModule,MatFormFieldModule,MatInputModule,MatButtonModule,
    AppRoutingModule,MatTabsModule, MatSelectModule,
    BrowserAnimationsModule,MatDialogModule
  ],
  entryComponents:[DialogBoxComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
