//dialog-box.component.ts
import { Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface UsersData {
  name: string;
  id: number;
}
export interface studentData{
  name:string;
  studentId:number;
  branch:string;
  emailId:string;
  district:string;
}

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent {

  action:string;
  local_data:any;
update:boolean=false;
delete:boolean=false;
add:boolean=false;
  constructor(
    public dialogRef: MatDialogRef<DialogBoxComponent>,
    //@Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: studentData) {
    console.log(data);
    this.local_data = {...data};
    console.log(this.local_data)
    this.action = this.local_data.action;
    if(this.action == 'Update'){
      this.update=true;
      this.delete=false;
      this.add=false;

    }
    else if(this.action == 'Delete'){
      this.update=false;
      this.delete=true;
      this.add=false;
    }
    else if(this.action == 'Add'){
      this.add=true;
      this.update=false;
      this.delete=false;
    }
    else  console.log(this.action);
    console.log(this.action);
  }

  doAction(){
    delete this.local_data.action;
    this.dialogRef.close({event:this.action,data:this.local_data});
    console.log(this.local_data);
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

}
