import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';

export interface UsersData {
  name: string;
  id: number;
}
export interface studentData{
  name:string;
  studentId:number;
  branch:string;
  emailId:string;
  district:string;
}

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  studentData:object[]=[
    {name:'santhosh',studentId:1234,branch:'eee',emailId:'santhosh@gmail.com',district:'warangal'},
    {name:'sudhakar',studentId:1235,branch:'eee',emailId:'sudhakar@gmail.com',district:'janagam'},
    {name:'sushanth',studentId:1236,branch:'eee',emailId:'sushanth@gmail.com',district:'vijayawada'},
    {name:'sampath',studentId:1237,branch:'eee',emailId:'sampath@gmail.com',district:'hyderabad'},
    {name:'srishanvi',studentId:1238,branch:'eee',emailId:'srishanvi@gmail.com',district:'khammam'},
    {name:'sushmitha',studentId:1239,branch:'eee',emailId:'sushmitha@gmail.com',district:'karimnagar'},
    {name:'shanmukh',studentId:1240,branch:'eee',emailId:'shanmukh@gmail.com',district:'adilabad'},
    {name:'subbu',studentId:1241,branch:'eee',emailId:'subbu@gmail.com',district:'warangal'},
    {name:'shiva',studentId:1242,branch:'eee',emailId:'shiva@gmail.com',district:'nalgonda'}
  ];
  displayedColumns1:string[]=['name','studentId','branch','emailId','district','action'];
  

  constructor(public dialog: MatDialog) {}
  showStudentData:boolean=false;
addedStudentData:studentData[]=[];
dataSource1= this.addedStudentData;
  openDialog1(action,obj) {
    console.log(action,obj);
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result.event,result.data);
      this.addedStudentData.push(result.data);
      this.showStudentData=true;
      console.log(this.addedStudentData);

      
    });
  }


    ngOnInit() {
  }

}
