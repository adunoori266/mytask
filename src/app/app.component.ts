import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

export interface UsersData {
  name: string;
  id: number;
}

const ELEMENT_DATA: UsersData[] = [
  {id: 1560608769632, name: 'Artificial Intelligence'},
  {id: 1560608796014, name: 'Machine Learning'},
  {id: 1560608787815, name: 'Robotic Process Automation'},
  {id: 1560608805101, name: 'Blockchain'}
];
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  //formgroup object to create a edit form
  studentDAta:FormGroup;

//hard coded student data to display in a table format
  studentData:object[]=[
    {name:'santhosh',studentId:1234,branch:'eee',emailId:'santhosh@gmail.com',district:'warangal'},
    {name:'sudhakar',studentId:1235,branch:'eee',emailId:'sudhakar@gmail.com',district:'janagam'},
    {name:'sushanth',studentId:1236,branch:'eee',emailId:'sushanth@gmail.com',district:'vijayawada'},
    {name:'sampath',studentId:1237,branch:'eee',emailId:'sampath@gmail.com',district:'hyderabad'},
    {name:'srishanvi',studentId:1238,branch:'eee',emailId:'srishanvi@gmail.com',district:'khammam'},
    {name:'sushmitha',studentId:1239,branch:'eee',emailId:'sushmitha@gmail.com',district:'karimnagar'},
    {name:'shanmukh',studentId:1240,branch:'eee',emailId:'shanmukh@gmail.com',district:'adilabad'},
    {name:'subbu',studentId:1241,branch:'eee',emailId:'subbu@gmail.com',district:'warangal'},
    {name:'shiva',studentId:1242,branch:'eee',emailId:'shiva@gmail.com',district:'nalgonda'}
  ];
  showStudentData:boolean=false;
  selectedStudentData:object;
  displayData(event){
    console.log(event);
    console.log('this method is called');
   for (const student of this.studentData) {
     if (student['studentId'] ===event.value) {
       this.selectedStudentData=student;
       this.showStudentData=true;
       
     }
     
   }
    // this.showStudentData=true;
    


  }
  dataToBeEdited:object={
    name:'',
    studentId:'',
    branch:'',
    emailId:'',
    district:''
  };
  //to store the index of data which is to edited
 indexOfDataToBeEdited:number;
 //this method called when edit button clicked and recieves the data and index which is to be edited
  editData(data:object,index:number){
    //storing the data
    this.dataToBeEdited=data;
    //storing index
    this.indexOfDataToBeEdited=index;
    //creating a form with the previous  stored student values
    this.studentDAta=this.fb.group({
      name:this.dataToBeEdited['name'],
      studentId:this.dataToBeEdited['studentId'],
      branch:this.dataToBeEdited['branch'],
      emailId:this.dataToBeEdited['emailId'],
      district:this.dataToBeEdited['district']
      

    });

    console.log(data,index);
  }
  clearData(){
    console.log('this is called');
    this.studentDAta.reset();
    this.studentDAta.markAsUntouched();
    
  }
  //to delete student data
  deleteData(index:number){
    //delteting the student data
    this.studentData.splice(index,1);
    console.log(index)
  }
  //updating the data of students with the new values
  submitData(){
    console.log(this.studentDAta.value);
    this.studentData.splice(this.indexOfDataToBeEdited,1,this.studentDAta.value);
  
  }
  addData(){

    this.studentData.push(this.studentDAta.value)
  }
  ngOnInit(){
    this.studentDAta=this.fb.group({
      name:'',
      studentId:'',
      branch:'',
      emailId:'',
      district:''
      

    });
    
    this.skillsForm=this.fb.group({
      name:'',
      skills:this.fb.array([
        this.fb.group({
          skill:'',
          exp:''
        })
      ])
    });
  }
  onSubmit(){
    console.log(this.skillsForm.value)
  }
  newSkill():FormGroup{
    return this.fb.group({
      skill:'',
      exp:''
    })

  }
  //getting skills
  get skills():FormArray{
    return this.skillsForm.get("skills") as FormArray

  }
  //addding new skills to formgroup
  addSkills(){
    this.skills.push(this.newSkill());
  }
  //removing skills
  removeSkill(i:number){
    this.skills.removeAt(i);

  }
  // creation of form model
  skillsForm:FormGroup;
  constructor(private fb:FormBuilder){
    

  }

  displayedColumns:string[]=['name','studentId','branch','emailId','district'];
  dataSource= this.studentData;
  title = 'formArray';
  
}
